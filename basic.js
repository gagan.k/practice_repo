//devloped in 1993 by brenden eich
//high level lang..
//runtime : browser, node.js
//v8 engine
//just in time compiler

//printing a  value to the console
console.log("Hello World");

//data types
let x  = 42;     //number
let y = "hello";   //string     
let z = true;    //boolean

//undefined and null are special data type called primitive data types
let u;          //undefined
let a= null;
//object is collection of key-value pairs

const human = {name: 'John', age: 30};
human["city"]="New York" ;

console.log(human);

//accessing object properties using dot notation or bracket notation
console.log(human.name)
console.log(human['age'])

//function  declaration
function addNumbers (a ,b){
    return a+ b;
}

console.log(addNumbers(5,7));

//function expression
let multiplyNumbers = function(c,d){
    return c * d;
}

console.log(multiplyNumbers(6,9))

//arrow functions
let square = (x)=> x*x;
console.log(square(8));

//this keyword
let person={
    name:"John",
    sayHello:function(){
        console.log(`Hi! My name is ${this.name}`);
    }
}
person.sayHello();

//call apply bind methods
person.sayHello.apply(person);
person.sayHello.bind(person)()

//higher order function 
let processData=function(data,processFunc){
    let processedData=processFunc(data);
    console.log(processedData);
}

//list  of numbers
let listOfNumbers=[1,2,3,4,5]

// creating map 
const dict = new Map(
    [['one', 1], ['two', 2], ['three', 3]]
  );

//callback  function
let findNumberInArray=(arr,cb)=>{
   for(let i=0;i<arr.length;i++){
       if(cb(arr[i])){
           return arr[i];
       }
   }
};
console.log(findNumberInArray(listOfNumbers,(n)=>n%2===0))

//closure
let counter=function(){
    let count=0;
    return ()=>count++
}
let increment=counter();
console.log(increment()); //outputs 0
console.log(increment()); //outputs 1

//immediately invoked function expression (IIFE)
((x)=>console.log(`${x} is a great number`))("7");

//object destructuring
let obj={a:1,b:2,c:3};

//synchronous  code
async function asyncfun(){
    const result= await Promise;
    console.log(result);
}
//try  catch finally
try {
     throw "error";
}catch(err){
    console.log(err);
}finally{
    console.log("this is finally block")
}
// write async  and await in try catch finally
async function asyncfun(){
    try{
    const result= await Promise;
    console.log(result);
    }
    catch(error){
        console.log(error);
    }
}

// importing  module using import statement
import helper from './other';
helper();

//exporting multiple values using export keyword


import {a,b} from './other'
console.log(a+b)

//node package  manager (npm)
//npm install  <package-name> : to install a package
//npm uninstall <package-name>: to remove a installed package
//npm list : shows the list of packages installed
//npm show <package-name>: gives details about that particular package
//npm version: gives the current node version
//npm -v : gives the current npm version
//npx create-react-app my-app : for creating react app
//to run script use : node filename.js or directly running filename.js

//use  typescript for type  checking while writing javascript code



